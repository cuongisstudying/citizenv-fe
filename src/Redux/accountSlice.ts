import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    locationName: undefined,
    role: undefined
};

export const accountSlice = createSlice({
  name: 'account',
  initialState,
  reducers: {
    update: (state, action) => {
      state.locationName = action.payload.at(0);
      state.role = action.payload.at(1);
    }
  }
});

export const accountActions = accountSlice.actions;
export const selectAccount = (state) => state.account;
export default accountSlice.reducer;
