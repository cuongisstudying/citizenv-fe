import { roleToAddChild } from 'user/UserRolses';
import API from '../../utils/api';

export const AUTH_URL = '/user';

export function getAll() {
  return API.get(`${AUTH_URL}/all`);
}
export function getChild() {
  return API.get(`${AUTH_URL}/getchild`);
}
export function declare(data) {
  const role = localStorage.getItem('role');
  const address = roleToAddChild(role);
  return API.post(`${address}/create`, data);
}

export function create(data) {
  return API.post(`${AUTH_URL}/create`, data);
}

export function login(username, password) {
  return API.post(`${AUTH_URL}/login`, { password, username });
}
export function updatePerm(receiverUsername, startTime, endTime) {
  return API.put(`${AUTH_URL}/setPerm`, { endTime, startTime, receiverUsername });
}
export function updateProgress(username, progress) {
  return API.put(`${AUTH_URL}/set_progress`, { progress, username });
}

export function getAccount(username) {
  return API.get(`${AUTH_URL}/wname?username=${username}`);
}
export function getCitizen() {
  return API.get(`${AUTH_URL}/getcitizen`);
}
export function getprogress() {
  return API.get(`${AUTH_URL}/progress`);
}
export function getSexChart() {
  return API.get(`${AUTH_URL}/sex_chart`);
}
export function getAgeChart() {
  return API.get(`${AUTH_URL}/age_chart`);
}
