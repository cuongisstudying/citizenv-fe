import { createSlice } from '@reduxjs/toolkit';

export interface AuthState {
  token: string;
  username: string;
}

const initialState: AuthState = {
  token: undefined,
  username: undefined,
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login(state, action) {
      state.token = action.payload.at(0);
      state.username = action.payload.at(1);

    },
    logout(state) {
      state = initialState;
    }
  }
});
// Acctions
export const authActions = authSlice.actions;

// Selector
export const selectUsername = (state) => state.auth.username;
export default authSlice.reducer;
