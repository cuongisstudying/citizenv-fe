import API from '../utils/api';

export const AUTH_URL = '/citizen';

export function create(data) {
  return API.post(`${AUTH_URL}/create`, data);
}
export function updateCitizen(data) {
  return API.put(`${AUTH_URL}/update`, data);
}
export function getCitizenById(id) {
  return API.get(`${AUTH_URL}/id-get/${id}`);
}
export function deleteCitizenByID(id) {
  return API.delete(`${AUTH_URL}/delete/${id}`);
}
