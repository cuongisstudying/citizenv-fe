import { configureStore } from '@reduxjs/toolkit';
import accountReducer from './accountSlice';
import authReducer from './auth/authSlice';
import { persistStore } from 'redux-persist';

export const store = configureStore({
  reducer: {
    account: accountReducer,
    auth: authReducer
  }
});


export default store;
export const persistor = persistStore(store);