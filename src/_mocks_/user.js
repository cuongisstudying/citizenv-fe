import { useState, useEffect } from 'react';
import faker from 'faker';
import { sample } from 'lodash';
// utils
import { mockImgAvatar } from '../utils/mockImages';
import { getAll } from '../Redux/auth/authCrud';

// ----------------------------------------------------------------------

export const GetUsers = () => {
  const [user, setUser] = useState({});
  useEffect(() => {
    getAll().then((res) => {
      setUser(res.data)
    })
  }, []);

  return user;
}

const users = [...Array(24)].map((_, index) => ({
  id: faker.datatype.uuid(),
  avatarUrl: mockImgAvatar(index + 1),
  username: faker.name.findName(),
  locationName: faker.company.companyName(),
  isVerified: faker.datatype.boolean(),
  status: sample(['active', 'banned']),
  role: sample([
    'Leader',
    'Hr Manager',
    'UI Designer',
    'UX Designer',
    'UI/UX Designer',
    'Project Manager',
    'Backend Developer',
    'Full Stack Designer',
    'Front End Developer',
    'Full Stack Developer'
  ])
}));

export default users;
