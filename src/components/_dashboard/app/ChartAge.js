// material
import { Box, Card, CardHeader } from '@mui/material';
import ReactApexChart from 'react-apexcharts';
import { styled } from '@mui/material/styles';
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------

const CHART_HEIGHT = 440;

const ChartWrapperStyle = styled(Box)(({ theme }) => ({
  height: CHART_HEIGHT,
}));

export default function ChartAge() {
  const series= [{
    name: 'Nam',
    data: [0.4, 0.65, 0.76, 0.88, 1.5, 2.1, 2.9, 3.8, 3.9, 4.2, 4, 4.3, 4.1, 4.2, 4.5,
      3.9, 3.5, 3
    ]
  },
  {
    name: 'Nữ',
    data: [-0.8, -1.05, -1.06, -1.18, -1.4, -2.2, -2.85, -3.7, -3.96, -4.22, -4.3, -4.4,
      -4.1, -4, -4.1, -3.4, -3.1, -2.8
    ]
  }
  ];
  const options = {
    chart: {
      type: 'bar',
      stacked: true
    },
    colors: ['#008FFB', '#FF4560'],
    plotOptions: {
      bar: {
        horizontal: true,
        barHeight: '80%',
      },
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      width: 1,
      colors: ["#fff"]
    },
    
    grid: {
      xaxis: {
        lines: {
          show: false
        }
      }
    },
    yaxis: {
      min: -5,
      max: 5,
      title: {
        // text: 'Age',
      },
    },
    tooltip: {
      shared: false,
      x: {
        formatter: function (any) {
          return any
        }
      },
      y: {
        formatter: function (val) {
          return Math.abs(val) + "%"
        }
      }
    },
    xaxis: {
      categories: ['85+', '80-84', '75-79', '70-74', '65-69', '60-64', '55-59', '50-54',
        '45-49', '40-44', '35-39', '30-34', '25-29', '20-24', '15-19', '10-14', '5-9',
        '0-4'
      ],
      labels: {
        formatter: function (val) {
          return Math.abs(Math.round(val)) + "%"
        }
      }
    }
  };
  return (
    <Card>
    <CardHeader title={'Tháp dân số Việt Nam 2020'} />
    <ChartWrapperStyle sx={{ mx: 3 }} dir="ltr">
      <ReactApexChart type="bar" series={series} options={options}  height={400} />
    </ChartWrapperStyle>
  </Card>
  );
}
