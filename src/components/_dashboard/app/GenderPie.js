import { merge } from 'lodash';
import ReactApexChart from 'react-apexcharts';
// material
import { useTheme, styled } from '@mui/material/styles';
import { Card, Box, CardHeader, CircularProgress } from '@mui/material';
// utils
import { fNumber } from '../../../utils/formatNumber';
//
import { BaseOptionChart } from '../../charts';
import { useEffect, useState } from 'react';
import { getSexChart } from 'Redux/auth/authCrud';

// ----------------------------------------------------------------------
const CHART_HEIGHT = 400;
const LEGEND_HEIGHT = 72;

const ChartWrapperStyle = styled('div')(({ theme }) => ({
  height: CHART_HEIGHT,
  marginTop: theme.spacing(5),
  '& .apexcharts-canvas svg': { height: CHART_HEIGHT },
  '& .apexcharts-canvas svg,.apexcharts-canvas foreignObject': {
    overflow: 'visible'
  },
  '& .apexcharts-legend': {
    height: LEGEND_HEIGHT,
    alignContent: 'center',
    position: 'relative !important',
    borderTop: `solid 1px ${theme.palette.divider}`,
    top: `calc(${CHART_HEIGHT - LEGEND_HEIGHT}px) !important`
  }
}));

// ----------------------------------------------------------------------


export default function GenderPie() {
  const theme = useTheme();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState([]);
  useEffect(() => {
    setLoading(true);
    getSexChart().then((res) => {
      const datana = res?.data?.data;
      setData(datana);
      setLoading(false);
    })
    .catch(() => {
      setLoading(false);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const chartOptions = merge(BaseOptionChart(), {
    colors: [theme.palette.info.main, theme.palette.secondary.main],
    labels: ['Nam', 'Nữ'],
    stroke: { colors: [theme.palette.background.paper] },
    legend: { floating: true, horizontalAlign: 'center' },
    dataLabels: { enabled: true, dropShadow: { enabled: false } },
    tooltip: {
      fillSeriesColor: false,
      y: {
        formatter: (seriesName) => fNumber(seriesName),
        title: {
          formatter: (seriesName) => `#${seriesName}`
        }
      }
    },
    plotOptions: {
      pie: { donut: { labels: { show: false } } }
    }
  });

  return (
    <Card>
      <CardHeader title="Tỉ lệ Nam/Nữ" />
      {loading ? (
        <Box sx={{ textAlign: 'center', my: 25 }}>
          <CircularProgress color="secondary" />
        </Box>
      ) : (
      <ChartWrapperStyle dir="ltr">
        <ReactApexChart type="pie" series={data} options={chartOptions} height={280}  />
      </ChartWrapperStyle>
      )}
    </Card>
  );
}
