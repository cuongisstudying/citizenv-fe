import ReactApexChart from 'react-apexcharts';
import { useState, useEffect } from 'react';
// material
import { Box, Card, CardHeader, CircularProgress } from '@mui/material';
import { getprogress } from 'Redux/auth/authCrud';
import { styled } from '@mui/material/styles';
// utils
// ----------------------------------------------------------------------

const CHART_HEIGHT = 420;

const ChartWrapperStyle = styled('div')(({ theme }) => ({
  height: CHART_HEIGHT
}));
export default function ProgressRates() {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState([]);
  const [name, setName] = useState('');
  const role = localStorage.getItem('role');
  useEffect(() => {
    setLoading(true);
    getprogress().then((res) => {
      const datana = res?.data?.data;
      setName(datana.name);
      if (role === 'A1') {
        setName('Toàn quốc');
        setData(datana);
      }
      if (role === 'A2') setData(datana.districts);
      if (role === 'A3') setData(datana.wards);
      if (role === 'B1') setData(datana.villages);
    });
    setLoading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  let initdata = [];
  let initcategories = [];

  if (data)
  // eslint-disable-next-line array-callback-return
  data.map((v) => {
    if (v.totalInput){
      initdata.push(v.totalInput)
      initcategories.push(v.name)
    }
  });
  const dat = [
    {
      name: 'Hiện tại',
      data: initdata
    }
  ];
  const options = {
    chart: {
      type: 'bar',
      height: 'auto'
    },
    plotOptions: {
      bar: {
        borderRadius: 4,
        horizontal: true,
        barHeight: '50%',
      }
    },
    colors: ['#FF8474', '#FFEAD6', '#9F5F80'],
    dataLabels: {
      enabled: false
    },
    xaxis: {
      categories:initcategories
    }
  }
  return (
    <Card height={700} >
      <CardHeader title={'Tiến độ nhập Dân số ' + name} subheader="Dơn vị: người" />
        {loading ? (
        <Box sx={{ textAlign: 'center', my: 25 }}>
          <CircularProgress color="secondary" />
        </Box>
      ) : (
      <ChartWrapperStyle sx={{ mx: 3 }} dir="ltr">
        <ReactApexChart type="bar" series={dat} options={options} height={420}  />
      </ChartWrapperStyle>
      )}
    </Card>
  );
}
