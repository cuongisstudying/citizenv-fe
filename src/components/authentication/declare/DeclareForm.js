import * as Yup from 'yup';
import { useFormik, Form, FormikProvider } from 'formik';
import Button from '@mui/material/Button';
import { purple } from '@mui/material/colors';
import { styled } from '@mui/material/styles';
import { toast } from 'react-toastify';
// material
import { Stack, TextField } from '@mui/material';
import { declare } from 'Redux/auth/authCrud';
// import { LoadingButton } from '@mui/lab';

// ----------------------------------------------------------------------

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(purple[500]),
  backgroundColor: '#9F5F80',
  '&:hover': {
    backgroundColor: '#B1427b'
  }
}));

export default function DeclareForm() {
  const DeclareSchema = Yup.object().shape({
    name: Yup.string().min(2, 'Quá ngắn!').max(50, 'Quá dài!').required('Thiếu tên đơn vị'),
    code: Yup.number().required('Chưa nhập  mã tài khoản').typeError('Mã tài khoản phải là số')
  });

  const formik = useFormik({
    initialValues: {
      name: '',
      code: ''
    },
    validationSchema: DeclareSchema,
    onSubmit: (values) => {
      declare(values)
      .then(() => {
        toast.success('Khai báo mã thành công');
      })
      .catch(() =>{
        toast.error('Khai báo mã Thất bại');
      })
      
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Stack spacing={4}>
          <TextField
            fullWidth
            label="Tên đơn vị"
            {...getFieldProps('name')}
            error={Boolean(touched.name && errors.name)}
            helperText={touched.name && errors.name}
          />

          <TextField
            fullWidth
            type="text"
            label="Mã tài khoản"
            {...getFieldProps('code')}
            error={Boolean(touched.code && errors.code)}
            helperText={touched.code && errors.code}
          />

          <ColorButton
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            loading={isSubmitting}
          >
            Khai báo tài khoản
          </ColorButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
