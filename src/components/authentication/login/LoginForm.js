import * as Yup from 'yup';
import { useState } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { useFormik, Form, FormikProvider } from 'formik';
import { toast } from 'react-toastify';

import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import Button from '@mui/material/Button';
import { purple } from '@mui/material/colors';
import { styled } from '@mui/material/styles';
import {
  Box,
  Link,
  Stack,
  TextField,
  IconButton,
  InputAdornment,
  CircularProgress
} from '@mui/material';
import { login } from '../../../Redux/auth/authCrud';

// material

// ----------------------------------------------------------------------
const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(purple[500]),
  backgroundColor: '#9F5F80',
  '&:hover': {
    backgroundColor: '#B1427b'
  }
}));
const Linked = styled(Link)({
  color: purple[500]
});
export default function LoginForm() {
  const [showPassword, setShowPassword] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const LoginSchema = Yup.object().shape({
    username: Yup.string().required('Chưa nhập mã tài khoản').typeError('Mã tài khoản phải là số'),
    password: Yup.string().required('Chưa nhập mật khẩu')
  });

  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
      remember: true
    },
    validationSchema: LoginSchema,
    onSubmit: (values) => {
      setIsLoading(true);
      login(values.username?.trim(), values.password?.trim())
        .then((res) => {
          const { token, username, role, permission, locationName, progress } = res.data;
          localStorage.setItem('accessToken', token);
          localStorage.setItem('username', username);
          localStorage.setItem('role', role);
          localStorage.setItem('locationName', locationName);
          localStorage.setItem('permission', permission);
          localStorage.setItem('progress', progress);
          setIsLoading(false);
          switch (role) {
            case 'A1':
            case 'A2':
            case 'A3': {
              navigate('/menu/analysis', { replace: true });
              break;
            }
            case 'B1':
            case 'B2': {
              navigate('/menu/input', { replace: true });
              break;
            }
            default: {
              navigate('/menu/citizen', { replace: true });
              break;
            }
          }
        })
        .catch(() => {
          formik.setSubmitting(false);
          toast.error('Tài khoản không tồn tại');
          setIsLoading(false);
        });
    }
  });

  const { errors, touched, isSubmitting, handleSubmit, getFieldProps } = formik;

  const handleShowPassword = () => {
    setShowPassword((show) => !show);
  };

  const navigate = useNavigate();

  return (
    <>
      {isLoading ? (
        <Box sx={{ textAlign: 'center', my: 10 }}>
          <CircularProgress color="secondary" />
        </Box>
      ) : (
        <FormikProvider value={formik}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <Stack spacing={3}>
              <TextField
                fullWidth
                autoComplete="username"
                type="text"
                label="Mã tài khoản"
                {...getFieldProps('username')}
                error={Boolean(touched.username && errors.username)}
                helperText={touched.username && errors.username}
              />

              <TextField
                fullWidth
                autoComplete="current-password"
                type={showPassword ? 'text' : 'password'}
                label="Mật khẩu"
                {...getFieldProps('password')}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton onClick={handleShowPassword} edge="end">
                        <Icon icon={showPassword ? eyeFill : eyeOffFill} />
                      </IconButton>
                    </InputAdornment>
                  )
                }}
                error={Boolean(touched.password && errors.password)}
                helperText={touched.password && errors.password}
              />
            </Stack>

            <Stack
              direction="row"
              alignItems="center"
              justifyContent="space-between"
              sx={{ my: 2 }}
            >
              <Linked component={RouterLink} variant="subtitle2" to="#">
                Quên mật khẩu?
              </Linked>
            </Stack>

            <ColorButton
              fullWidth
              size="large"
              type="submit"
              variant="contained"
              loading={isSubmitting}
            >
              Đăng nhập
            </ColorButton>
          </Form>
        </FormikProvider>
      )}
    </>
  );
}
