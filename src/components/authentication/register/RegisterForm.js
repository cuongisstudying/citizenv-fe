import * as Yup from 'yup';
import { useState } from 'react';
import { Icon } from '@iconify/react';
import { useFormik, Form, FormikProvider } from 'formik';
import eyeFill from '@iconify/icons-eva/eye-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import Button from '@mui/material/Button';
import { purple } from '@mui/material/colors';
import { styled } from '@mui/material/styles';
import { toast } from 'react-toastify';
// material
import { Stack, TextField, IconButton, InputAdornment } from '@mui/material';
// import { LoadingButton } from '@mui/lab';
import { create } from '../../../Redux/auth/authCrud';

// ----------------------------------------------------------------------

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(purple[500]),
  backgroundColor: '#9F5F80',
  '&:hover': {
    backgroundColor: '#B1427b'
  }
}));

export default function RegisterForm() {
  const [showPassword, setShowPassword] = useState(false);

  const RegisterSchema = Yup.object().shape({
    locationName: Yup.string().min(2, 'Quá ngắn!').max(50, 'Quá dài!').required('Thiếu tên đơn vị'),
    username: Yup.number().required('Chưa nhập  mã tài khoản').typeError('Mã tài khoản phải là số'),
    password: Yup.string().required('Yêu cầu mật khẩu')
  });

  const formik = useFormik({
    initialValues: {
      locationName: '',
      username: '',
      password: ''
    },
    validationSchema: RegisterSchema,
    onSubmit: (values) => {
      create(values)
        .then(() => {
          toast.success('Cấp tài khoản thành công');
        })
        .catch(() =>{
          toast.error('Cấp tài khoản Thất bại');
        })
        
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Stack spacing={4}>
          <TextField
            fullWidth
            label="Tên đơn vị"
            {...getFieldProps('locationName')}
            error={Boolean(touched.locationName && errors.locationName)}
            helperText={touched.locationName && errors.locationName}
          />

          <TextField
            fullWidth
            type="text"
            label="Mã tài khoản"
            {...getFieldProps('username')}
            error={Boolean(touched.username && errors.username)}
            helperText={touched.username && errors.username}
          />

          <TextField
            fullWidth
            type={showPassword ? 'text' : 'password'}
            label="Mật khẩu"
            {...getFieldProps('password')}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton edge="end" onClick={() => setShowPassword((prev) => !prev)}>
                    <Icon icon={showPassword ? eyeFill : eyeOffFill} />
                  </IconButton>
                </InputAdornment>
              )
            }}
            error={Boolean(touched.password && errors.password)}
            helperText={touched.password && errors.password}
          />

          <ColorButton
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            loading={isSubmitting}
          >
            Tạo tài khoản
          </ColorButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
