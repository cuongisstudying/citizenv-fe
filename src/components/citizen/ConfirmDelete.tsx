import React, { useState } from 'react';
import { toast } from 'react-toastify';
import { Modal } from 'react-bootstrap';
import { Icon } from '@iconify/react';
// import DatePicker from '@mui/lab/DatePicker';
// import { Stack, TextField, Typography } from '@mui/material';
import { Button, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import trash2Outline from '@iconify/icons-eva/trash-2-outline';
import {  deleteCitizenByID } from 'Redux/citizenCrud';
const ConfirmDelete: React.FC<{
  open: any,
  id: any,
  data: any,
  close: () => void,
  setChange: () => void
}> = ({ close, open, id, setChange, data }) => {
  const idcode = id;
  const [loading, setLoading] = useState(false);

  function handleClick() {
    setLoading(true);
    deleteCitizenByID(idcode)
      .then((res) => {
        setChange();
        toast.success("Đã xoá");
        setLoading(false);

      })
      .catch(() => {
        setLoading(false);
      });
  }

  return (
    <div>
      <Modal
        style={{ left: "150px" }}
        show={open}
        onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title >Xoá hồ sơ </Modal.Title>
        </Modal.Header>
        <Modal.Body >
          <Typography variant="subtitle1" align="center">Xác nhận xoá hồ sơ của {data.name} </Typography>
        </Modal.Body>
        <Modal.Footer>
          <Button color="secondary" variant="contained" onClick={close}>
            Huỷ
          </Button>
          <LoadingButton
            color="primary"
            onClick={handleClick}
            loading={loading}
            loadingPosition="start"
            variant="contained"
            startIcon={<Icon icon={trash2Outline} width={24} height={24} />}
          >
            Xoá
          </LoadingButton>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default ConfirmDelete;
