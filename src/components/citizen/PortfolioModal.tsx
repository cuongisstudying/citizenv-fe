import React from 'react';
import { Modal } from 'react-bootstrap';
import { Stack, Typography } from '@mui/material';
import Box from '@mui/material/Box';
// import DatePicker from '@mui/lab/DatePicker';
import Button from '@mui/material/Button';

const PortfolioModal: React.FC<{
  open: any,
  name: any,
  data: any
  close: () => void
}> = ({ open, close, name, data }) => {
  const convert = (date) => {
    if(date){
      let nDate = date.slice(8, 10) + '/' 
      + date.slice(5, 7) + '/' 
      + date.slice(0, 4);
      return nDate;
    }
    else return '';
  }

  const {identityNumber, birthday, inputLocation, sex, originAddress, permanentAddress, tempAddress, religion, educationalLevel, job}= data;
  return (
    <div>
      {/* <Button onClick={close}>Huỷ</Button> */}
      <Modal
        style={{ left: "150px" }}
        size="lg"
        show={open}
        onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title >Hồ sơ chi tiết của {name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Stack spacing={3} alignItems="left">
          <Stack direction="row" spacing={2}>
              <Typography component="div">
                <Box sx={{ fontWeight: 'bold' }}>Số CMND/CCCD:</Box>
              </Typography>
              <Typography>{identityNumber>10 ? identityNumber : 'Chưa được cấp'}</Typography>
            </Stack>

            <Stack direction="row" spacing={2}>
              <Typography component="div">
                <Box sx={{ fontWeight: 'bold' }}>Họ và tên:</Box>
              </Typography>
              <Typography>{name}</Typography>
            </Stack>

            <Stack direction="row" spacing={2}>
              <Typography component="div">
                <Box sx={{ fontWeight: 'bold' }}>Ngày sinh:</Box>
              </Typography>
              <Typography>{convert(birthday)}</Typography>
            </Stack>

            <Stack direction="row" spacing={2}>
              <Typography component="div">
                <Box sx={{ fontWeight: 'bold' }}>Giới tính:</Box>
              </Typography>
              <Typography>{sex}</Typography>
            </Stack>

            <Stack direction="row" spacing={2}>
              <Typography component="div">
                <Box sx={{ fontWeight: 'bold' }}>Quê quán:</Box>
              </Typography>
              <Typography>{originAddress}</Typography>
            </Stack>

            <Stack direction="row" spacing={2}>
              <Typography component="div">
                <Box sx={{ fontWeight: 'bold' }}>Địa chỉ:</Box>
              </Typography>
              <Typography>{inputLocation}</Typography>
            </Stack>

            <Stack direction="row" spacing={2}>
              <Typography component="div">
                <Box sx={{ fontWeight: 'bold' }}>Địa chỉ thường trú:</Box>
              </Typography>
              <Typography>{permanentAddress}</Typography>
            </Stack>

            <Stack direction="row" spacing={2}>
              <Typography component="div">
                <Box sx={{ fontWeight: 'bold' }}>Địa chỉ tạm trú:</Box>
              </Typography>
              <Typography>{tempAddress}</Typography>
            </Stack>

            <Stack direction="row" spacing={2}>
              <Typography component="div">
                <Box sx={{ fontWeight: 'bold' }}>Tôn giáo:</Box>
              </Typography>
              <Typography>{religion}</Typography>
            </Stack>

            <Stack direction="row" spacing={2}>
              <Typography component="div">
                <Box sx={{ fontWeight: 'bold' }}>Trình độ văn hóa:</Box>
              </Typography>
              <Typography>{educationalLevel}</Typography>
            </Stack>

            <Stack direction="row" spacing={2}>
              <Typography component="div">
                <Box sx={{ fontWeight: 'bold' }}>Nghề nghiệp:</Box>
              </Typography>
              <Typography>{job}</Typography>
            </Stack>
          </Stack>
        </Modal.Body>
        <Modal.Footer>
          <Button color="secondary" variant="contained" onClick={close}>
            Đóng
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default PortfolioModal;
