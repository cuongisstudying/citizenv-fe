import React from 'react';
import { Modal } from 'react-bootstrap';
import { Stack } from '@mui/material';

import { InputForm } from 'components/input';

const UpdateModal: React.FC<{
  open: any,
  id: any,
  data: any,
  close: () => void,
  setChange: () => void
}> = ({ close, open, id, setChange, data }) => {

  return (
    <div>
      <Modal
        style={{ left: "150px" }}
        size="lg"
        show={open}
        onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title >Chỉnh sửa hồ sơ của {data.name} </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Stack spacing={3} alignItems="left">
            <InputForm data={data} update={true} close={close} setChange={setChange}/>
          </Stack>
        </Modal.Body>
        {/* <Modal.Footer>
        <Button color="secondary" variant="contained" onClick={close}>
            Đóng
          </Button>
        </Modal.Footer> */}
      </Modal>
    </div>
  );
};

export default UpdateModal;
