import React, { useState, useEffect } from 'react';
import { Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';
import * as Yup from 'yup';

import { useFormik, Form, FormikProvider } from 'formik';
import viLocale from 'date-fns/locale/vi';

import Button from '@mui/material/Button';
// material
import {
  Stack,
  TextField,
  FormControlLabel,
  RadioGroup,
  FormLabel,
  FormControl,
  Radio
} from '@mui/material';
import SaveIcon from '@mui/icons-material/Save';
import LoadingButton from '@mui/lab/LoadingButton';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';

import { create } from '../../Redux/citizenCrud';
import { updateCitizen } from '../../Redux/citizenCrud';

// import { LoadingButton } from '@mui/lab';

// ----------------------------------------------------------------------

const convert = (date) => {
  if (date) {
    let result = date.substring(0, 19)
    result += ".000Z"
    return result;
  }
  else return null;
}

const InputForm: React.FC<{
  data: any,
  update: boolean,
  close: () => void,
  setChange: () => void,
}> = ({ data, update, close, setChange }) => {
  const [birth, setBirth] = useState(new Date('2021-12-25T00:09:00.000Z'));
  const [sex, setSex] = useState('');
  const [updating, setUpdating] = useState(false);

  useEffect(() => {
    if (data) {
      const { birthday, sex } = data;
      let birthdaycv = convert(birthday);
      setBirth(birthdaycv);
      setSex(sex);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);
  function incrementDate(dateInput) {
    // var increment = 1;
    // var dateFormatTotime = new Date(dateInput);
    // var increasedDate = new Date(dateFormatTotime.getTime() + (increment * 86400000));
    // return increasedDate;
    return dateInput;
  }
  const handleUpdate = () => {
    const birthday =incrementDate(birth);
    const data = { ...formik.values, birthday: birthday, sex: sex }
    setUpdating(true)
    updateCitizen(data)
      .then((res) => {
        setChange();
        toast.success("Thay đổi thành công");
        close();
        setUpdating(false);

      })
      .catch(() => {
        console.log("update false")
        setUpdating(false);
      });

  }
  const handleSex = (event) => {
    setSex(event.target.value);
  }


  const RegisterSchema = Yup.object().shape({
    name: Yup.string().required('Không được để trống tên'),
    originAddress: Yup.string().required('Không được để trống quê quán'),
    // identityNumber: Yup.number().typeError('Số CMND/CCCD phải là số'),
    // permanentAddress: Yup.string().required('Không được để trống địa chỉ thường trú')
  });
  const formik = useFormik({
    initialValues: data ? data : {
      educationalLevel: '',
      identityNumber: null,
      inputUsername: '',
      job: '',
      name: '',
      originAddress: '',
      permanentAddress: '',
      religion: '',
      tempAddress: ''
    },
    validationSchema: RegisterSchema,
    onSubmit: (values) => {
      setUpdating(true);
      const birthday =incrementDate(birth);
      create({ ...values, birthday: birthday, sex: sex })
        .then(() => {
          toast.success('Tạo thành công hồ sơ mới');
          setUpdating(false);
        })
        .catch((e) => {
          formik.setSubmitting(false);
          toast.error("Không thể tạo hồ sơ");
          setUpdating(false);
        });
    }
  });

  const { errors, touched, handleSubmit, getFieldProps } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Stack spacing={3}>
          <TextField
            fullWidth
            type="number"
            label="Số CMND/CCCD (Để trống nếu chưa được cấp)"
            {...getFieldProps('identityNumber')}
           // helperText={touched.identityNumber && errors.identityNumber}
          />

          <TextField
            fullWidth
            type="text"
            label="Họ và tên *"
            {...getFieldProps('name')}
            error={Boolean(touched.name && errors.name)}
            helperText={touched.name && errors.name}
          />

          <LocalizationProvider dateAdapter={AdapterDateFns} locale={viLocale}>
            <DatePicker
              views={['day', 'month', 'year']}
              label="Ngày sinh"
              value={birth}
              onChange={(newValue) => {
                newValue.setHours(9);
                setBirth(newValue);
              }}
              renderInput={(params) => <TextField {...params} helperText={null} />}
            />
          </LocalizationProvider>

          <FormControl component="fieldset">
            <FormLabel component="legend">Giới tính *</FormLabel>
            <RadioGroup
              row
              aria-label="gender"
              name="row-radio-buttons-group"
              value={sex}
              onChange={handleSex}
            >
              <FormControlLabel value="Nữ" control={<Radio />} label="Nữ" />
              <FormControlLabel value="Nam" control={<Radio />} label="Nam" />
              <FormControlLabel value="Khác" control={<Radio />} label="Khác" />
            </RadioGroup>
          </FormControl>

          <TextField
            fullWidth
            type="text"
            label="Quê quán *"
            {...getFieldProps('originAddress')}
            error={Boolean(touched.originAddress && errors.originAddress)}
            helperText={touched.originAddress && errors.originAddress}
          />

          <TextField
            fullWidth
            type="text"
            label="Địa chỉ thường trú (Để trống sẽ đặt địa chỉ mặc định)"
            {...getFieldProps('permanentAddress')}
          />

          <TextField
            fullWidth
            type="text"
            label="Địa chỉ tạm trú (Để trống nếu trùng với địa chỉ thường trú)"
            {...getFieldProps('tempAddress')}
            error={Boolean(touched.tempAddress && errors.tempAddress)}
            helperText={touched.tempAddress && errors.tempAddress}
          />

          <TextField
            fullWidth
            type="text"
            label="Tôn giáo"
            {...getFieldProps('religion')}
            error={Boolean(touched.religion && errors.religion)}
            helperText={touched.religion && errors.religion}
          />

          <TextField
            fullWidth
            type="text"
            label="Trình độ văn hóa"
            {...getFieldProps('educationalLevel')}
            error={Boolean(touched.educationalLevel && errors.educationalLevel)}
            helperText={touched.educationalLevel && errors.educationalLevel}
          />

          <TextField
            fullWidth
            type="text"
            label="Nghề nghiệp"
            {...getFieldProps('job')}
            error={Boolean(touched.job && errors.job)}
            helperText={touched.job && errors.job}
          />
          {update ? (
            <Modal.Footer>
              <Button color="secondary" variant="contained" onClick={close}>
                Huỷ
              </Button>
              <LoadingButton
                color="primary"
                // onClick={handleClick}
                loading={updating}
                loadingPosition="start"
                onClick={handleUpdate}
                variant="contained"
                startIcon={<SaveIcon />}
              >
                lưu
              </LoadingButton>
            </Modal.Footer>
          ) : (
            <LoadingButton
              color="primary"
              // onClick={handleClick}
              loading={updating}
              loadingPosition="start"
              variant="contained"
              type="submit"
              startIcon={<SaveIcon />}
            >
              Xác nhận
            </LoadingButton>
          )}


        </Stack>
      </Form>
    </FormikProvider>
  );
}
export default InputForm;