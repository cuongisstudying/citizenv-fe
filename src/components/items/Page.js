import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet-async';
import { forwardRef } from 'react';
import Link from '@mui/material/Link';
// material
import { Box, Typography, Stack } from '@mui/material';

// ----------------------------------------------------------------------

function Copyright() {
  return (
    <>
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="#">
        Hoàng Hữu Tùng
      </Link>{', '}
      <Link color="inherit" href="#">
        Đặng Trung Cương
      </Link>{' và '}
      <Link color="inherit" href="#">
        Lê Thị Minh Hồng
      </Link>{' '}
    </Typography>
    <Typography variant="body2" color="textSecondary" align="center">
      Lớp phát triển ứng dụng web 2121I_INT3306_21  -  Thầy Lê Đình Thanh  -  trường Đại học Công Nghệ
    </Typography>
    </>
  );
}
const Page = forwardRef(({ children, title = '', ...other }, ref) => (
  <Stack ref={ref} {...other}>
    <Helmet>
      <title>{title}</title>
    </Helmet>
    {children}
    <Box pt={20} pb={2} >
      <Copyright />
    </Box>
  </Stack>
));

Page.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string
};

export default Page;
