import PropTypes from 'prop-types';
// material
import { Paper, Typography } from '@mui/material';

// ----------------------------------------------------------------------

SearchNotFound.propTypes = {
  searchQuery: PropTypes.string
};

export default function SearchNotFound({ searchQuery = '', ...other }) {
  return (
    <Paper {...other}>
      <Typography gutterBottom align="center" variant="subtitle1">
        Không tìm thấy
      </Typography>
      <Typography variant="body2" align="center">
        Không thấy kết quả nào cho &nbsp;
        <strong>&quot;{searchQuery}&quot;</strong>. Kiểm tra lại lỗi đánh máy và nhập đúng giá trị cần tìm.
      </Typography>
    </Paper>
  );
}
