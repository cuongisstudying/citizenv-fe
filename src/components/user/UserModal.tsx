import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { Modal } from 'react-bootstrap';
// import DatePicker from '@mui/lab/DatePicker';
import { Stack, TextField, Typography } from '@mui/material';
import { Button } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import DesktopDateRangePicker from '@mui/lab/DesktopDateRangePicker';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import viLocale from 'date-fns/locale/vi';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import Box from '@mui/material/Box';
import SaveIcon from '@mui/icons-material/Save';
import { DateRange } from '@mui/lab/DateRangePicker';
import { updatePerm } from 'Redux/auth/authCrud';

const UserModal: React.FC<{
  open: any,
  code: any,
  start: any,
  end: any,
  setIsChange: () => void,
  close: () => void
}> = ({ open, code, close, setIsChange, start, end }) => {
  const convert = (date) => {
    if (date) {
      // 2000-02-08T02:00:00Z
      // 2000-02-08T02:00:00.000Z
      // 2021-11-30T17:00:31.000Z
      let result = date.substring(0, 11)
      result += "02:00:00.000Z"
      return result;
    }
    else return null;
  }

  start = new Date(convert(start));
  end = new Date(convert(end));

  // const [startDay, setStartDay] =useState(new Date());
  // const [endDay, setEndDay] = useState(new Date());
  const [loading, setLoading] = useState(false);
  const [value, setValue] = useState<DateRange<Date>>([start, end]);

  useEffect(() => {
    // let date = new Date(start)
    // setStartDay(convert(start));
    setValue([start, end])
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);
  function incrementDate(dateInput) {
    var increment =1;
    var dateFormatTotime = new Date(dateInput);
    var increasedDate = new Date(dateFormatTotime.getTime() +(increment *86400000));
    return increasedDate;
}
  function handleClick() {
    setLoading(true);
    const started = incrementDate(value.at(0))
    const ended = incrementDate(value.at(1))
    console.log(started);

    updatePerm(code, started, ended)
    .then((res) => {
      toast.success("Thay đổi đã được lưu");
      if (end !== value.at(1)) setIsChange();
      setLoading(false);
      close();
    })
    .catch(() => {
      setLoading(false);
      toast.error("Đã có lỗi xảy ra");
    });
  }

  return (
    <div>
      <Modal
        style={{ left: "150px" }}
        size="lg"
        show={open}
        onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title >Chỉnh sửa quyền khai báo cho tài khoản {code}</Modal.Title>
        </Modal.Header>
        <Modal.Body>

          <Stack spacing={4} alignItems="center">
            <Typography variant="body1" sx={{ mt: 2 }}>
              Chỉnh sửa bắt đầu và ngày kết thúc quá trình khai báo
            </Typography>
            <LocalizationProvider dateAdapter={AdapterDateFns} locale={viLocale}>
              <DesktopDateRangePicker
                startText="Ngày bắt đầu"
                endText="Ngày kết thúc"
                value={value}
                onChange={(newValue) => {
                  setValue(newValue);
                }}
                renderInput={(startProps, endProps) => (
                  <React.Fragment>
                    <TextField {...startProps} />
                    <Box sx={{ mx: 2 }}> Đến </Box>
                    <TextField {...endProps} />
                  </React.Fragment>
                )}
              />
            </LocalizationProvider>
          </Stack>

        </Modal.Body>
        <Modal.Footer>
          <Button color="secondary" variant="contained" onClick={close}>
            Đóng
          </Button>
          <LoadingButton
            color="primary"
            onClick={handleClick}
            loading={loading}
            loadingPosition="start"
            variant="contained"
            startIcon={<SaveIcon />}
          >
            Lưu thay đổi
          </LoadingButton>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default UserModal;
