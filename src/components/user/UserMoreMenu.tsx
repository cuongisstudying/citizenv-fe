import { Icon } from '@iconify/react';
import React, { useState, useRef } from 'react';
import editFill from '@iconify/icons-eva/edit-fill';
// import { Link as RouterLink } from 'react-router-dom';
import trash2Outline from '@iconify/icons-eva/trash-2-outline';
import moreVerticalFill from '@iconify/icons-eva/more-vertical-fill';
// material
import { Menu, MenuItem, IconButton, ListItemIcon, ListItemText } from '@mui/material';
import ConfirmDelete from 'components/citizen/ConfirmDelete';
import UpdateModal from 'components/citizen/UpdateModal';

// ----------------------------------------------------------------------

const UserMoreMenu: React.FC<{
  open: any,
  id: any,
  data: any,
  setChange: () => void,
  close: () => void
}> = ({ close, open, id, setChange, data }) => {
  const ref = useRef(null);
  const [isOpen, setIsOpen] = useState(false);
  const [confirm, setConfirm] = useState(false);
  const [update, setUpdate] = useState(false);

  const handleConfirm = () => {
    setIsOpen(false);
    setConfirm(true);
  }
  const handleUpdate = () => {
    setIsOpen(false);
    setUpdate(true);
  }

  return (
    <>
      <IconButton ref={ref} onClick={() => setIsOpen(true)}>
        <Icon icon={moreVerticalFill} width={20} height={20} />
      </IconButton>

      <Menu
        open={isOpen}
        anchorEl={ref.current}
        onClose={() => setIsOpen(false)}
        PaperProps={{
          sx: { width: 200, maxWidth: '100%' }
        }}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      >
        <MenuItem onClick={handleConfirm} sx={{ color: 'text.secondary' }}>
          <ListItemIcon>
            <Icon icon={trash2Outline} width={24} height={24} />
          </ListItemIcon>
          <ListItemText primary="Xoá" primaryTypographyProps={{ variant: 'body2' }} />
        </MenuItem>

        <MenuItem onClick={handleUpdate} sx={{ color: 'text.secondary' }}>
          <ListItemIcon>
            <Icon icon={editFill} width={24} height={24} />
          </ListItemIcon>
          <ListItemText primary="Chỉnh sửa" primaryTypographyProps={{ variant: 'body2' }} />
        </MenuItem>
      </Menu>
      <ConfirmDelete
        id={id}
        data={data}
        open={confirm}
        close={() => setConfirm(false)}
        setChange={() => setChange()} />
      <UpdateModal
        id={id}
        data={data}
        open={update}
        close={() => setUpdate(false)}
        setChange={() => setChange()} />
    </>
  );
}
export default UserMoreMenu;