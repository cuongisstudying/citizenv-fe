import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
// material
import { styled } from '@mui/material/styles';
import { Button, Box, Drawer, Typography, Avatar, Stack } from '@mui/material';
// components
import Logo from '../../components/items/Logo';
import NavSection from '../../components/items/NavSection';
import { MHidden } from '../../components/@material-extend';
import Label from '../../components/items/Label';
//
import { sidebarConfigA, sidebarConfigB1, sidebarConfigB2 } from './SidebarConfig';
import acc from '../../_mocks_/account';
import { authActions } from '../../Redux/auth/authSlice';
import { roleToLoca, roleToPosi } from '../../user/UserRolses';

// ----------------------------------------------------------------------

const DRAWER_WIDTH = 280;

const RootStyle = styled('div')(({ theme }) => ({
  [theme.breakpoints.up('lg')]: {
    flexShrink: 0,
    width: DRAWER_WIDTH
  }
}));

const AccountStyle = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(2, 2.5),
  borderRadius: theme.shape.borderRadiusSm,
  backgroundColor: theme.palette.grey[200]
}));

// ----------------------------------------------------------------------

DashboardSidebar.propTypes = {
  isOpenSidebar: PropTypes.bool,
  onCloseSidebar: PropTypes.func
};

export default function DashboardSidebar({ isOpenSidebar, onCloseSidebar }) {
  let permission = localStorage.getItem('permission');
  let sidebarConfig = sidebarConfigA;
  let perm = true;
  if (permission === 'false') perm = false;
  const username = localStorage.getItem('username');
  const role = localStorage.getItem('role');
  const locationName = localStorage.getItem('locationName');
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { pathname } = useLocation();
  if (role === 'B2') sidebarConfig = sidebarConfigB2;
  if (role === 'B1') sidebarConfig = sidebarConfigB1;

  // useEffect(() => {
  //   getAccount(username).then((res) => {
  //     const { locationName, role } = res.data.data;
  //     const data = [locationName, role];
  //     dispatch(accountActions.update(data));
  //   });
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [username]);
  useEffect(() => {
    if (isOpenSidebar) {
      onCloseSidebar();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);
  const handleLogout = () => {
    dispatch(authActions.logout());
    localStorage.removeItem('accessToken');
    localStorage.removeItem('username');
    localStorage.removeItem('role');
    localStorage.removeItem('permission');
    localStorage.removeItem('locationName');
    localStorage.removeItem('progress');

    navigate('/login', { replace: true });
  };

  const renderContent = (
    <Stack
      sx={{ height: '100%', mt: 2 }}
      direction="column"
      justifyContent="space-between"
      alignItems="stretch"
      spacing={2}
    >
      <Box>
        <Stack
          sx={{ py: 2 }}
          direction="column"
          justifyContent="center"
          alignItems="center"
          fullWidth
        >
          <Box component={RouterLink} to="/menu/citizen">
            <Logo />
          </Box>
        </Stack>

        <Box sx={{ mb: 1, mx: 2.5 }}>
          <Stack spacing={1}>
            <AccountStyle>
              <Avatar src={acc.photoURL} alt="photoURL" />
              <Box sx={{ ml: 2 }}>
                <Typography variant="body2" sx={{ color: 'text.secondary' }}>
                  {roleToLoca(role)}
                </Typography>
                <Typography variant="subtitle2" sx={{ color: 'text.primary' }}>
                  {locationName + ': ' + username}
                </Typography>
                <Typography variant="body2" sx={{ color: 'text.secondary' }}>
                  {roleToPosi(role)}
                </Typography>
              </Box>
            </AccountStyle>
            <AccountStyle>
              <Stack direction="row" spacing={4} alignItems="center">
                <Typography variant="body2" sx={{ color: 'text.secondary' }}>
                  Quyền Chỉnh sửa/ khai báo:
                </Typography>
                <Label variant="ghost" color={perm ? 'success' : 'error'}>
                  {perm ? 'MỞ' : 'ĐÓNG'}
                </Label>
              </Stack>
            </AccountStyle>
          </Stack>
        </Box>
        <NavSection navConfig={sidebarConfig} />
      </Box>

      <Box sx={{ p: 2 }}>
        <Button
          sx={{ mb: 1, py: 1.5 }}
          fullWidth
          color="primary"
          variant="contained"
          onClick={handleLogout}
        >
          <strong>Đăng xuất</strong>
        </Button>
      </Box>
    </Stack>
  );

  return (
    <RootStyle>
      <MHidden width="lgUp">
        <Drawer
          open={isOpenSidebar}
          onClose={onCloseSidebar}
          PaperProps={{
            sx: { width: DRAWER_WIDTH }
          }}
        >
          {renderContent}
        </Drawer>
      </MHidden>

      <MHidden width="lgDown">
        <Drawer
          open
          variant="persistent"
          PaperProps={{
            sx: {
              width: DRAWER_WIDTH,
              bgcolor: 'background.default'
            }
          }}
        >
          {renderContent}
        </Drawer>
      </MHidden>
    </RootStyle>
  );
}
