import { Icon } from '@iconify/react';
import pieChart2Fill from '@iconify/icons-eva/pie-chart-2-fill';
import peopleFill from '@iconify/icons-eva/people-fill';
import fileTextFill from '@iconify/icons-eva/file-text-fill';
import barChartFill from '@iconify/icons-eva/bar-chart-fill';
import personAddFill from '@iconify/icons-eva/person-add-fill';
import bxsUserAccount from '@iconify/icons-bx/bxs-user-account';
// import alertTriangleFill from '@iconify/icons-eva/alert-triangle-fill';

// ----------------------------------------------------------------------

const getIcon = (name) => <Icon icon={name} width={22} height={22} />;

export const sidebarConfigA = [
  {
    title: 'Phân tích',
    path: '/menu/analysis',
    icon: getIcon(pieChart2Fill)
  },
  {
    title: 'Dân số',
    path: '/menu/citizen',
    icon: getIcon(peopleFill)
  },
  {
    title: 'Quản lý tài khoản',
    path: '/menu/user',
    icon: getIcon(bxsUserAccount)
  },
  {
    title: 'Khai báo',
    path: '/menu/declare',
    icon: getIcon(fileTextFill)
  },
  {
    title: 'Cấp tài khoản',
    path: '/menu/create',
    icon: getIcon(personAddFill)
  }
];

export const sidebarConfigB1 = [
  {
    title: 'Báo cáo tiến độ',
    path: '/menu/progress',
    icon: getIcon(barChartFill)
  },
  {
    title: 'Phân tích',
    path: '/menu/analysis',
    icon: getIcon(pieChart2Fill)
  },
  {
    title: 'Dân số',
    path: '/menu/citizen',
    icon: getIcon(peopleFill)
  },
  {
    title: 'Quản lý tài khoản',
    path: '/menu/user',
    icon: getIcon(bxsUserAccount)
  },
  {
    title: 'Khai báo',
    path: '/menu/declare',
    icon: getIcon(fileTextFill)
  },
  {
    title: 'Cấp tài khoản',
    path: '/menu/create',
    icon: getIcon(personAddFill)
  },
  {
    title: 'Nhập liệu',
    path: '/menu/input',
    icon: getIcon(fileTextFill)
  }
];

export const sidebarConfigB2 = [
  {
    title: 'Dân số',
    path: '/menu/citizen',
    icon: getIcon(peopleFill)
  },
  {
    title: 'Nhập liệu',
    path: '/menu/input',
    icon: getIcon(fileTextFill)
  }
];

