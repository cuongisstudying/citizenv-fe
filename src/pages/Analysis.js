// material
import { Box, Grid, Container, Typography } from '@mui/material';
import AgePie from 'components/_dashboard/app/AgePie';
import ChartAge from 'components/_dashboard/app/ChartAge';
import GenderPie from 'components/_dashboard/app/GenderPie';
// components
import Page from '../components/items/Page';
import {
  ProgressRates
} from '../components/_dashboard/app';


// ----------------------------------------------------------------------

export default function Analysis() {
  return (
    <Page title="Phân tích | CitizenV">
      <Container maxWidth="xl">
        <Box sx={{ pb: 5 }}>
          <Typography variant="h4">Tổng hợp và phân tích số liệu dân số</Typography>
        </Box>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={8}>
            <ChartAge/>
          </Grid>
          <Grid item xs={12} md={6} lg={4}>
            <GenderPie/>
          </Grid>
          <Grid item xs={12} md={6} lg={4}>
            <AgePie/>
          </Grid>
          <Grid item xs={12} md={12} lg={8}>
            <ProgressRates />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
}
