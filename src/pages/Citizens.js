import { filter } from 'lodash';
import { useState, useEffect } from 'react';
// material
import {
  Card,
  Table,
  Stack,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  CircularProgress
} from '@mui/material';
// components
import Page from '../components/items/Page';
import Label from '../components/items/Label';
import Scrollbar from '../components/items/Scrollbar';
import SearchNotFound from '../components/items/SearchNotFound';
import { UserListHead, UserListToolbar, UserMoreMenu } from '../components/user';
import { getCitizen } from '../Redux/auth/authCrud';
// import { deleteCitizenById, updateCitizen, getCitizenById } from '../Redux/citizenCrud';
import PortfolioModal from 'components/citizen/PortfolioModal';

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'Họ và tên', alignHead: -1 },
  { id: 'identityNumber', label: 'CMND/CCCD', alignHead: -1 },
  { id: 'sex', label: 'Giới tính', alignHead: 0 },
  { id: 'birthday', label: 'Ngày sinh', alignHead: 0 },
  { id: 'originAddress', label: 'Quê quán', alignHead: -1 },
  { id: 'inputLocation', label: 'Địa chỉ', alignHead: -1 },
  { id: 'status', label: 'Hồ sơ', alignHead: 0 },
  { id: '' }
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(array, (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function Citizens() {
  let perm = localStorage.getItem('permission');
  let user = localStorage.getItem('username');
  let permission = true;
  if (perm === 'false') permission = false;

  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [citizenList, setCitizenList] = useState([]);
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState([]);
  const [name, setName] = useState('');
  const [change, setChange] = useState(false);

  const handleOpen = (data, name) => {
    setData(data);
    setName(name);
    setOpen(true);
  };

  useEffect(() => {
    setIsLoading(true);
    getCitizen()
      .then((res) => {
        const data = res?.data?.data;
        if (data) {
          setCitizenList(data);
        }
        setIsLoading(false);
        setChange(false);
      })
      .catch(() => {
        setIsLoading(false);
        setChange(false);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [change]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = citizenList.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const convert = (date) => {
    if (date) {
      let nDate = date.slice(8, 10) + '/' + date.slice(5, 7) + '/' + date.slice(0, 4);
      return nDate;
    } else return '';
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - citizenList.length) : 0;

  const filteredUsers = applySortFilter(citizenList, getComparator(order, orderBy), filterName);

  const isUserNotFound = filteredUsers.length === 0;

  return (
    <Page title="Dân số | CitizenV">
      <Container>
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4" gutterBottom>
            Danh sách dân số
          </Typography>
        </Stack>

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <UserListHead
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={citizenList.length}
                  numSelected={selected.length}
                  onRequestSort={handleRequestSort}
                  onSelectAllClick={handleSelectAllClick}
                  align="center"
                />
                <TableBody>
                  {filteredUsers
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      const { id, identityNumber, inputLocation, name, originAddress, sex, birthday, inputUsername } =
                        row;
                      const isItemSelected = selected.indexOf(name) !== -1;

                      return (
                        <TableRow
                          hover
                          key={id}
                          tabIndex={-1}
                          role="checkbox"
                          selected={isItemSelected}
                          aria-checked={isItemSelected}
                        >
                          <TableCell padding="checkbox">
                            <Checkbox
                              checked={isItemSelected}
                              onChange={(event) => handleClick(event, name)}
                            />
                          </TableCell>
                          <TableCell component="th" scope="row" padding="none">
                            <Typography variant="subtitle2" noWrap sx={{ ml: 2 }}>
                              {name}
                            </Typography>
                          </TableCell>
                          <TableCell align="left">{identityNumber>10 ? identityNumber : 'Chưa được cấp'}</TableCell>
                          <TableCell align="center">{sex}</TableCell>
                          <TableCell align="center">{birthday ? convert(birthday) : ''}</TableCell>
                          <TableCell align="left">{originAddress}</TableCell>
                          <TableCell align="left">{inputLocation}</TableCell>
                          <TableCell align="center">
                            <Label
                              variant="ghost"
                              color="secondary"
                              cursor="pointer"
                              onClick={() => handleOpen(row, name)}
                              sx={{ ':hover': { boxShadow: 6 } }}
                            >
                              Xem Hồ sơ
                            </Label>
                          </TableCell>

                          <TableCell align="right">
                            {(permission & (user===inputUsername)) ? (
                              <UserMoreMenu data={row} id={id} setChange={() => setChange(true)} />
                            ) : (
                              <div />
                            )}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {!isLoading ? (
                  isUserNotFound && (
                    <TableBody>
                      <TableRow>
                        <TableCell align="center" colSpan={12} sx={{ py: 3 }}>
                          <SearchNotFound searchQuery={filterName} />
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  )
                ) : (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={12} sx={{ py: 3 }}>
                        <CircularProgress color="primary" />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component="div"
            count={citizenList.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      <PortfolioModal data={data} name={name} open={open} close={() => setOpen(false)} />
    </Page>
  );
}
