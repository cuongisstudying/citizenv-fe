// material
import { styled } from '@mui/material/styles';
import { Box, Container, Typography } from '@mui/material';
// layouts
// components
import Page from '../components/items/Page';
import { RegisterForm } from '../components/authentication/register';

// import AuthSocial from '../components/authentication/AuthSocial';

// ----------------------------------------------------------------------

const RootStyle = styled(Page)(({ theme }) => ({
  [theme.breakpoints.up('md')]: {
    display: 'flex'
  }
}));

// const SectionStyle = styled(Card)(({ theme }) => ({
//   width: '100%',
//   maxWidth: 464,
//   display: 'flex',
//   flexDirection: 'column',
//   justifyContent: 'center',
//   margin: theme.spacing(2, 0, 2, 2)
// }));

const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '20vh',
  flexDirection: 'column',
  justifyContent: 'center',
  // padding: theme.spacing(12, 0)
}));

// ----------------------------------------------------------------------

export default function CreateAccount() {
  // check if token is in store if not navigate to login else continue
  return (
    <RootStyle title="Cấp tài khoản | CitizenV">
      <Container>
        <ContentStyle>
          <Box sx={{ mb: 5 }}>
            <Box sx={{ mb: 15 }}/>
            <Typography variant="h4" gutterBottom>
              Cấp tài khoản cho đơn vị trực thuộc.
            </Typography>
            <Typography sx={{ color: 'text.secondary' }}>
              Điền đầy đủ thông tin tài khoản của đơn vị trực thuộc.
            </Typography>
          </Box>
          <RegisterForm />
        </ContentStyle>
      </Container>
    </RootStyle>
  );
}
