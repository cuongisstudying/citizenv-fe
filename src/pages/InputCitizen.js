// material
import { styled } from '@mui/material/styles';
import { Box, Container, Typography, Stack, Button, Link } from '@mui/material';
import downloadFill from '@iconify/icons-eva/download-fill';
import { Icon } from '@iconify/react';
// layouts
// components
import Page from '../components/items/Page';
import { InputForm } from '../components/input';

// ----------------------------------------------------------------------

const RootStyle = styled(Page)(({ theme }) => ({
  [theme.breakpoints.up('md')]: {
    display: 'flex'
  }
}));

const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '20vh',
  flexDirection: 'column',
  justifyContent: 'center'
  // padding: theme.spacing(12, 0)
}));

// ----------------------------------------------------------------------

export default function InputCitizen() {
  return (
    <RootStyle title="Nhập liệu | CitizenV">
      <Container>
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4" gutterBottom></Typography>
          <Button
            color="primary"
            variant="contained"
            component={Link}
            href="https://bit.ly/citizenV-input"
            target="_blank"
            rel="noopener"
            startIcon={<Icon icon={downloadFill} />}
            sx={{
              bottom: '-10px',
              minHeight: '30px',
              '&:hover': { color: '#ebffed !important' }
            }}
          >
            Tải mẫu Phiếu điều tra
          </Button>
        </Stack>
        <ContentStyle>
          <Typography variant="h4" gutterBottom>
            Tạo hồ sơ mới
          </Typography>
          <Typography sx={{ color: 'text.secondary' }}>Các trường có dấu * là bắt buộc</Typography>
          <Box sx={{ my: 5 }}>
            <InputForm />
          </Box>
        </ContentStyle>
      </Container>
    </RootStyle>
  );
}
