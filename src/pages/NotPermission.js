import { Link as RouterLink } from 'react-router-dom';
import { motion } from 'framer-motion';
// material
import { Box, Button, Typography, Container } from '@mui/material';
// layouts
import { styled } from '@mui/material/styles';
// components
import { MotionContainer, varBounceIn } from '../components/animate';
import Page from '../components/items/Page';

// import AuthSocial from '../components/authentication/AuthSocial';

// ----------------------------------------------------------------------

const RootStyle = styled(Page)(({ theme }) => ({
  [theme.breakpoints.up('md')]: {
    display: 'flex'
  }
}));


const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '20vh',
  flexDirection: 'column',
  justifyContent: 'center',
  // padding: theme.spacing(12, 0)
}));

// ----------------------------------------------------------------------

export default function NotPermission() {
  return (
    <RootStyle title="Tài khoản chưa được cấp quyền | CitizenV">
      <Container>
        <ContentStyle>
        <MotionContainer initial="initial" open>
          <Box sx={{ margin: 'auto', textAlign: 'center' }}>
            <motion.div variants={varBounceIn}>
              <Typography mt={4} variant="h4" paragraph >
              Tài khoản chưa được mở quyền truy cập hoặc đã hết thời gian nhập liệu, 
              vui lòng liên hệ cấp trên để mở lại tài khoản 
              </Typography>
            </motion.div>

            <motion.div variants={varBounceIn}>
              <Box
                component="img"
                src="/static/illustrations/illustration_404.svg"
                sx={{ height: 260, mx: 'auto', my: { xs: 5, sm: 10 } }}
              />
            </motion.div>

            <Button to="/" size="large" variant="contained" component={RouterLink}>
              Quay lại trang chủ
            </Button>
          </Box>
        </MotionContainer>
        </ContentStyle>
      </Container>
    </RootStyle>
  );
}
