import React, { useState } from 'react';
import { toast } from 'react-toastify';
// material
import {
  Box,
  Stack,
  Card,
  Container,
  Typography,
  FormControlLabel,
  RadioGroup,
  FormLabel,
  FormControl,
  Popover,
  Radio
} from '@mui/material';
import { styled } from '@mui/material/styles';
import SaveIcon from '@mui/icons-material/Save';
import LoadingButton from '@mui/lab/LoadingButton';

// components
import Page from '../components/items/Page';
import Label from '../components/items/Label';
import { updateProgress } from 'Redux/auth/authCrud';
// ----------------------------------------------------------------------

const ContentStyle = styled(Card)(({ theme }) => ({
  minWidth: 480,
  margin: 'auto',
  padding: 'auto',
  display: 'flex',
  minHeight: '20vh',
  flexDirection: 'column',
  justifyContent: 'center',
  [theme.breakpoints.down('sm')]: {
    minWidth: 300
    // padding: theme.spacing(12, 0)
  }
}));
export default function Progress() {
  let progress = localStorage.getItem('progress');
  const username = localStorage.getItem('username');
  const [prog, setProg] = useState(progress);
  const [updating, setUpdating] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const handleProg = (event) => {
    setProg(event.target.value);
  };
  const handleUpdate = () => {
    setUpdating(true);
    let progress = parseInt(prog);
    updateProgress(username, progress)
      .then((res) => {
        localStorage.removeItem('progress');
        localStorage.setItem('progress', progress);
        toast.success('Cập nhật tiến độ thành công');
        setUpdating(false);
      })
      .catch(() => {
        console.log('update false');
        setUpdating(false);
      });
  };

  return (
    <Page title="Báo cáo tiến độ | CitizenV">
      <Container maxWidth="xl" center>
        <Box sx={{ pb: 5 }}>
          <Typography variant="h4">Báo cáo tiến độ nhập liệu</Typography>
        </Box>
        <Stack spacing={4} sx={{ mt: 15 }}>
          <ContentStyle>
            <FormControl sx={{ p: 6 }} component="fieldset">
              <FormLabel
                component="legend"
                aria-owns={open ? 'mouse-over-popover' : undefined}
                aria-haspopup="true"
                onMouseEnter={handlePopoverOpen}
                onMouseLeave={handlePopoverClose}
              >
                Báo cáo tiến độ
              </FormLabel>
              <RadioGroup
                aria-label="progress"
                name="radio-buttons-group"
                value={prog}
                onChange={handleProg}
              >
                <FormControlLabel value="1" control={<Radio />} label="Hoàn thành mức 1" />
                <FormControlLabel value="2" control={<Radio />} label="Đã hoàn thành" />
              </RadioGroup>
            </FormControl>
            <LoadingButton
              color="primary"
              // onClick={handleClick}
              loading={updating}
              loadingPosition="start"
              onClick={handleUpdate}
              variant="contained"
              startIcon={<SaveIcon />}
            >
              lưu
            </LoadingButton>
          </ContentStyle>
          <Popover
            id="mouse-over-popover"
            sx={{
              pointerEvents: 'none'
            }}
            open={open}
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'center'
            }}
            transformOrigin={{
              vertical: 'bottom',
              horizontal: 'center'
            }}
            onClose={handlePopoverClose}
            disableRestoreFocus
          >
            <ContentStyle>
              <Stack spacing={2} sx={{ m: 3 }}>
                <Box spacing={4}>
                  <Label variant="ghost" color="info">
                    Hoàn thành mức 1
                  </Label>
                  <Typography variant="content">
                    {' '}
                    Đã phát phiếu điều tra dân số và thu thập lại
                  </Typography>
                </Box>
                <Box spacing={4}>
                  <Label variant="ghost" color="success">
                    Đã hoàn thành
                  </Label>
                  <Typography variant="content"> Đã hoàn thành nhập liệu</Typography>
                </Box>
              </Stack>
            </ContentStyle>
          </Popover>
        </Stack>
      </Container>
    </Page>
  );
}
