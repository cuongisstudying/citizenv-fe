import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import { useState, useEffect } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
import personAddFill from '@iconify/icons-eva/person-add-fill';
import { Link as RouterLink } from 'react-router-dom';
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  CircularProgress
} from '@mui/material';
// components
import Page from '../components/items/Page';
import Label from '../components/items/Label';
import Scrollbar from '../components/items/Scrollbar';
import SearchNotFound from '../components/items/SearchNotFound';
import { UserListHead, UserListToolbar } from '../components/user';
import { getChild } from '../Redux/auth/authCrud';
import UserModal from 'components/user/UserModal';

// ----------------------------------------------------------------------
const TABLE_HEAD = [
  { id: 'username', label: 'Mã tài Khoản', alignHead: 1 },
  { id: 'locationName', label: 'Đơn vị', alignHead: -1 },
  { id: 'role', label: 'Chức vụ', alignHead: 0 },
  { id: 'start', label: 'Ngày bắt đầu', alignHead: 0 },
  { id: 'end', label: 'Ngày kết thúc', alignHead: 0 },
  { id: 'progress', label: 'Tiến độ', alignHead: 0 },
  { id: 'status', label: 'Quyền chỉnh sửa', alignHead: 0 }
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.username.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function User() {
  let permission = localStorage.getItem('permission');
  let perm = true;
  if (permission === 'false') perm = false;

  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('username');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [userList, setUserList] = useState([]);
  const [open, setOpen] = useState(false);
  const [code, setCode] = useState('');
  const [start, setStart] = useState('');
  const [end, setEnd] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isChange, setIsChange] = useState(false);

  const handleOpen = (code, start, end) => {
    if (perm) {
      setOpen(true);
      setCode(code);
      setStart(start);
      setEnd(end);
    }
  };
  useEffect(() => {
    setIsLoading(true);
    getChild()
      .then((res) => {
        const data = res?.data?.data;
        if (data) {
          setUserList(data);
        }
        setIsLoading(false);
        setIsChange(false);
      })
      .catch(() => {
        setIsLoading(false);
        setIsChange(false);
      });
  }, [isChange]);

  useEffect(() => {
    setIsLoading(true);
    getChild()
      .then((res) => {
        const data = res?.data?.data;
        if (data) {
          setUserList(data);
        }
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = userList.map((n) => n.username);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, username) => {
    const selectedIndex = selected.indexOf(username);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, username);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };
  const convert = (date) => {
    if (date) {
      let nDate = date.slice(8, 10) + '/' + date.slice(5, 7) + '/' + date.slice(0, 4);
      return nDate;
    } else return '';
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - userList.length) : 0;

  const filteredUsers = applySortFilter(userList, getComparator(order, orderBy), filterName);

  const isUserNotFound = filteredUsers.length === 0;
  const user = localStorage.getItem('username');

  return (
    <Page title="Quản lý tài khoản | CitizenV">
      <Container>
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4" gutterBottom>
            Danh sách tài khoản được quản lý bởi {user}
          </Typography>
          <Stack direction="row" spacing={2}>
            <Button
              color="secondary"
              variant="contained"
              component={RouterLink}
              to="/menu/declare"
              startIcon={<Icon icon={plusFill} />}
              sx={{
                minWidth: 100,
                '&:hover': { color: '#ebffed !important' }
              }}
            >
              Khai báo
            </Button>
            <Button
              mx={3}
              variant="contained"
              component={RouterLink}
              to="/menu/create"
              startIcon={<Icon icon={personAddFill} />}
              sx={{
                minWidth: 100,
                '&:hover': { color: '#ebffed !important' }
              }}
            >
              Cấp mã
            </Button>
          </Stack>
        </Stack>

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <UserListHead
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={userList.length}
                  numSelected={selected.length}
                  onRequestSort={handleRequestSort}
                  onSelectAllClick={handleSelectAllClick}
                  align="center"
                />
                <TableBody>
                  {filteredUsers
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      const {
                        id,
                        username,
                        role,
                        startTime,
                        endTime,
                        permission,
                        progress,
                        locationName
                      } = row;
                      const isItemSelected = selected.indexOf(username) !== -1;
                      const prog = parseInt(progress);

                      return (
                        <TableRow
                          hover
                          key={id}
                          tabIndex={-1}
                          role="checkbox"
                          selected={isItemSelected}
                          aria-checked={isItemSelected}
                        >
                          <TableCell padding="checkbox">
                            <Checkbox
                              checked={isItemSelected}
                              onChange={(event) => handleClick(event, username)}
                            />
                          </TableCell>
                          <TableCell component="th" scope="row" padding="none">
                            <Typography variant="subtitle2" noWrap sx={{ ml: 2 }}>
                              {username}
                            </Typography>
                          </TableCell>
                          <TableCell align="left">{locationName}</TableCell>
                          <TableCell align="center">{role}</TableCell>
                          <TableCell align="center">{convert(startTime)}</TableCell>
                          <TableCell align="center">{convert(endTime)}</TableCell>
                          <TableCell align="center">
                            <Label
                              variant="ghost"
                              color={prog === 0 ? 'error' : prog === 1 ? 'info' : 'success'}
                            >
                              {prog === 0 ? 'Đang bàn giao' : prog === 1 ? 'Hoàn thành mức 1' : 'Đã hoàn thành'}
                            </Label>
                          </TableCell>
                          <TableCell align="center">
                            <Label
                              variant="ghost"
                              color={permission ? 'success' : 'error'}
                              onClick={() => handleOpen(username, startTime, endTime)}
                              cursor="pointer"
                              sx={{ ':hover': { boxShadow: 6 } }}
                            >
                              {permission ? 'MỞ' : 'ĐÓNG'}
                            </Label>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {!isLoading ? (
                  isUserNotFound && (
                    <TableBody>
                      <TableRow>
                        <TableCell align="center" colSpan={12} sx={{ py: 3 }}>
                          <SearchNotFound searchQuery={filterName} />
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  )
                ) : (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={12} sx={{ py: 3 }}>
                        <CircularProgress color="primary" />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component="div"
            count={userList.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      <UserModal
        code={code}
        start={start}
        end={end}
        open={open}
        close={() => setOpen(false)}
        setIsChange={() => setIsChange(true)}
      />
    </Page>
  );
}
