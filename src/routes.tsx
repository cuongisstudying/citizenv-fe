import { Navigate, useRoutes } from 'react-router-dom';
// layouts
import DashboardLayout from './layouts/dashboard';
//
import Login from './pages/Login';
import CreateAccount from './pages/Createaccount';
import Declare from './pages/Declare';
import DashboardApp from './pages/DashboardApp';
import User from './pages/User';
import NotFound from './pages/Page404';
import InputCitizen from 'pages/InputCitizen';
import Citizens from 'pages/Citizens';
import NotPermission from 'pages/NotPermission';
import Progress from 'pages/Progress';
import Analysis from 'pages/Analysis';

// ----------------------------------------------------------------------

export default function Router() {
  let token = localStorage.getItem('accessToken');
  let perm = localStorage.getItem('permission');
  let permission = true;
  if (perm === 'false') permission = false;
  let isLoggedIn = false;
  if (token) {
    isLoggedIn = true
  }
  return useRoutes([
    {
      path: '/menu',
      element: isLoggedIn ? <DashboardLayout /> : <Navigate to="/login" />,
      children: [
        { path: 'app', element: <DashboardApp /> },
        { path: 'user', element: <User /> },
        { path: 'create', element: permission ? <CreateAccount /> : <NotPermission /> },
        { path: 'declare', element: permission ? <Declare /> : <NotPermission /> },
        { path: 'input', element: permission ? <InputCitizen /> : <NotPermission /> },
        { path: 'citizen', element: <Citizens /> },
        { path: 'progress', element: <Progress /> },
        { path: 'analysis', element: <Analysis /> },
      ]
    },
    {
      path: '/',
      element: <Login /> ,
      children: [
        { path: 'login', element: <Login /> },
        { path: '404', element: <NotFound /> },
        { path: '*', element: <Navigate to="/404" /> }
      ]
    },
    { path: '*', element: <Navigate to="/404" replace /> }
  ]);
}
