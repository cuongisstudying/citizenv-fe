export const role = [
  { value: 'A1', label: 'Bộ Y tế' },
  { value: 'A2', label: 'Tỉnh/thành' },
  { value: 'A3', label: 'Quận/huyện' },
  { value: 'B1', label: 'Xã/phường' },
  { value: 'B2', label: 'Thôn/bản/tổ dân phố' },

];
export const roleToLoca = (role: string) => {
  switch (role) {
    case 'A1':
      return 'Bộ Y tế';
    case 'A2':
      return 'Tỉnh/thành phố';
    case 'A3':
      return 'Quận/huyện';
    case 'B1':
      return 'Xã/phường';
    case 'B2':
      return 'Thôn/bản/tổ dân phố';
    default:
      break;
  }
};

export const roleToPosi = (role: string) => {
  switch (role) {
    case 'A1':
      return 'Tổng cục Dân số';
    case 'A2':
      return 'Chi cục dân số';
    case 'A3':
      return 'Công chức thực hiện công tác dân số';
    case 'B1':
      return 'Viên chức dân số';
    case 'B2':
      return 'Cộng tác viên dân số';
    default:
      break;
  }
};

export const roleToAddChild = (role: string) => {
  switch (role) {
    case 'A1':
      return 'province';
    case 'A2':
      return 'district';
    case 'A3':
      return 'ward';
    case 'B1':
      return 'village';
    default:
      break;
  }
};



// export const emailConfirmedOptions = [
//   { value: true, label: Translate('verified', 'Đã xác nhận') },
//   { value: false, label: Translate('verifying', 'Chưa xác nhận') },
// ];

// value={user.roles?.map((role) =>
//   roleOptions.find((op) => op.value === role),
// )}

