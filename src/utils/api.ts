import axios from 'axios';
import { toast } from 'react-toastify';
import { EnhancedStore } from '@reduxjs/toolkit';

const axiosInstance = axios.create({
  baseURL: 'https://citizenv-be.herokuapp.com/api/v1',
  responseType: 'json'
});

export const setupAxios = (store: EnhancedStore) => {

  const requestHandler = (request) => {
    const token = localStorage.getItem('accessToken')

    if (token) {
      request.headers.Authorization = token;
    }

    return request;
  };

  const successHandler = (response) => {
    if (response.data.success) return response;
    else {
      console.log(response.data.message)
    }
  };

  const errorHandler = (error) => {
    const errorRes = error.response;
    if (errorRes) {
      if (errorRes.status === 401) {
        // store.dispatch(actions.logout());
      }
      showError({ error: errorRes.data || {}, status: errorRes.status });
      if(errorRes.statusText === "Unauthorized") {
        localStorage.removeItem('accessToken');
        localStorage.removeItem('username');
        window.location.reload();
      }
  
    } else {
      toast.error('Có một lỗi không mong muốn đã xảy ra');
    }
    return Promise.reject(error);
  };

  const showError = ({ error, status }) => {
    // let title = i18n.t('AbpAccount::DefaultErrorMessage');
    let message = 'Có lỗi xảy ra!';
    if (typeof error === 'string') {
      message = error;

    } else if (error.details) {
      message = error.details;
    } else if (error.message) {
      message = error.message;
    } else {
      switch (status) {
        case 401:
          // title = i18n.t('AbpAccount::DefaultErrorMessage401');
          message = 'Bạn cần đăng nhập để thực hiện chức năng này!'; // message = i18n.t('AbpAccount::DefaultErrorMessage401Detail');
          break;
        case 403:
          // title = i18n.t('AbpAccount::DefaultErrorMessage403');
          message = 'Bạn không thể thực hiện chức năng này!'; // message = i18n.t('AbpAccount::DefaultErrorMessage403Detail');
          break;
        case 404:
          // title = i18n.t('AbpAccount::DefaultErrorMessage404');
          message = 'Không tồn tại!'; // message = i18n.t('AbpAccount::DefaultErrorMessage404Detail');
          break;
        case 500:
          // title = i18n.t('AbpAccount::500Message');
          message = 'Có lỗi xảy ra!'; // message = i18n.t('AbpAccount::InternalServerErrorMessage');
          break;
        default:
          break;
      }
    }
    console.log(`${message}`);
  };

  axiosInstance.interceptors.request.use((request) => requestHandler(request));

  axiosInstance.interceptors.response.use(
    (response) => successHandler(response),
    (error) => errorHandler(error)
  );
};

export default axiosInstance;
